# CHaMP 
**C**an I **Ha**ve **M**etabolites, **P**lease?

## Description
CHaMP is an RShiny app that loads a list of compound masses from twin-peaks analysis, and build a mass difference network from which is extracted the connected component containing a mass of interest. It provides a network visualization of the sub-network that encompass potential metabolic and spontaneous reactions that leads to degradation/metabolization products.

## Usage
```
R -e "shiny::runApp('app.R')"
```

## Contributing
Pull requests are welcome **on the gitlab repo** ([forgemia.inra.fr/metexplore/tools/degradation-products-search](https://forgemia.inra.fr/metexplore/tools/degradation-products-search)). For major changes, please open an issue first to discuss what you would like to change.  


## Authors and acknowledgment
Clément Frainay and Marine Valleix  
INRAE TOXALIM  
Research Centre in Food Toxicology  

## License
CHaMP is distributed under the open license [CeCILL-2.1](https://cecill.info/licences/Licence_CeCILL_V2.1-en.html) (compatible GNU-GPL).  


